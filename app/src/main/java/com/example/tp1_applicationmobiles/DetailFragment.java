package com.example.tp1_applicationmobiles;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import data.Country;

public class DetailFragment extends Fragment {

    Context c;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        c = container.getContext();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int id = args.getCountryId();
        String uri = Country.countries[id].getImgUri();
        ImageView flag = (ImageView)view.findViewById(R.id.flag);
        flag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));
        TextView capital = (TextView)view.findViewById(R.id.capital);
        TextView money =  (TextView)view.findViewById(R.id.Money);
        TextView name =  (TextView)view.findViewById(R.id.name);
        TextView pop =  (TextView)view.findViewById(R.id.pop);
        TextView superficie =  (TextView)view.findViewById(R.id.superficie);
        TextView langue =  (TextView)view.findViewById(R.id.langues);
        capital.setText(Country.countries[id].getCapital());
        money.setText(Country.countries[id].getCurrency());
        name.setText(Country.countries[id].getName());
        pop.setText(String.valueOf(Country.countries[id].getPopulation()));
        superficie.setText(String.valueOf(Country.countries[id].getArea()));
        langue.setText(Country.countries[id].getLanguage());
        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}